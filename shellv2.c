#include<fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "PUCITshell:- "
char** IO(char** arglist);
int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
int main(){
   char *cmdline;
   
   char** arglist;
   char* prompt = PROMPT;
   signal(SIGINT,SIG_IGN);  
     
while(1){
   
    cmdline = read_cmd(prompt,stdin);
   
    //exit(0);
      if((arglist = tokenize(cmdline)) != NULL){
   
    execute(arglist);
       //  need to free arglist
         for(int j=0; j < MAXARGS+1; j++)
             free(arglist[j]);
         free(arglist);
//for(int j=0; j < MAXARGS+1; j++)
       //      free(IOarr[j]);
         //free(IOarr);
         free(cmdline);
      }
  }//end of while loop
   printf("\n");
   return 0;
}
int execute(char* arglist[]){
   int status;
int i=0;while(arglist[i]){printf("%s",arglist[i]);i++;}
   int cpid = fork();
   switch(cpid){
      case -1:
         perror("fork failed");
          exit(1);
      case 0:   
        printf("case0\n");
            signal(SIGINT,SIG_DFL);
        IO(arglist);       
          printf("outside if\n");
          int i=0;while(arglist[i]){printf("%s",arglist[i]);i++;}
          execvp(arglist[0], arglist);
           perror("Command not found...");
          exit(1);
      default:
          waitpid(cpid, &status, 0);
         printf("child exited with status %d \n", status >> 8);
         return 0;
   }
}
char** tokenize(char* cmdline){
//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
       arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
      
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
//printf(arglist[1]);
//printf(arglist[3]);
 // if(strcmp(arglist[1],"0<")==0)
 //    {    printf("hello\n");
     //    strcpy(IOarr[0],arglist[2]);
    //    strcpy(IOarr[1],arglist[4]);
    //}
 
   arglist[argnum] = NULL;
   return arglist;
}     

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  int c; //input character
   int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
      break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0)
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}

char** IO(char** arglist){

if(strcmp(arglist[1],"0<")==0)
        { printf("1st if\n");
            if(strcmp(arglist[3],"1>")==0)
         {   
            printf("2ND IF\n");
            printf("2ND IF\n");
            //int i=0;while(arglist[i]){printf(arglist[i]);i++;}
            int fd1,fd2;
            fd1=open(arglist[2],O_RDONLY);
            fd2=open(arglist[4],O_WRONLY);
            dup2(fd1,0);dup2(fd2,1);
            strcpy(arglist[1],arglist[2]);
            strcpy(arglist[2],arglist[4]);
            strcpy(arglist[3],arglist[4]);
                arglist[5]=NULL;
        }           
        }
return arglist;
}